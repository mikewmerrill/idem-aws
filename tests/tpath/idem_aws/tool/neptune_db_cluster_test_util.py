def assert_neptune_db_cluster(
    hub,
    resource,
    db_cluster_name,
    engine,
    backup_retention_period,
    deletion_protection,
    tags,
):
    assert db_cluster_name == resource.get("name")
    assert engine == resource.get("engine")
    assert backup_retention_period == resource.get("backup_retention_period")
    assert deletion_protection == resource.get("deletion_protection")
    assert tags == resource.get("tags")


async def check_update_cluster(
    hub,
    ctx,
    db_cluster_name,
    resource_id,
    engine,
    backup_retention_period,
    deletion_protection,
    tags,
):
    ret = await hub.states.aws.neptune.db_cluster.present(
        ctx,
        name=db_cluster_name,
        resource_id=resource_id,
        engine=engine,
        backup_retention_period=backup_retention_period,
        deletion_protection=deletion_protection,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert_neptune_db_cluster(
        hub,
        resource,
        db_cluster_name,
        engine,
        backup_retention_period,
        deletion_protection,
        tags,
    )
