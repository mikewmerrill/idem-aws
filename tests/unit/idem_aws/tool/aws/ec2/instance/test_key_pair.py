import contextlib
import os
import tempfile

import pytest

SSH_PUBLIC_KEY = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAAAgQDhOql/5WMrC0jsf0xvpJXDaPPxMGKX0vvxc6ahgUU4kKaohAHGj+OCjHuq+7Qc6AZzKsg+0l/oBhdna79wXOB3SIHHHerm4ib6CS7hnCpRjYAfv8bf9FZlZcSq0in+Cfw6LHrvf1wbT30LodSPyImfQ6ojcNXT4CVpjVFZwAWrTw== test@localstack"

SSH_PRIVATE_KEY = """-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAIEA4Tqpf+VjKwtI7H9Mb6SVw2jz8TBil9L78XOmoYFFOJCmqIQBxo/j
gox7qvu0HOgGcyrIPtJf6AYXZ2u/cFzgd0iBxx3q5uIm+gku4ZwqUY2AH7/G3/RWZWXEqt
Ip/gn8Oix6739cG099C6HUj8iJn0OqI3DV0+AlaY1RWcAFq08AAAIIIhn9FCIZ/RQAAAAH
c3NoLXJzYQAAAIEA4Tqpf+VjKwtI7H9Mb6SVw2jz8TBil9L78XOmoYFFOJCmqIQBxo/jgo
x7qvu0HOgGcyrIPtJf6AYXZ2u/cFzgd0iBxx3q5uIm+gku4ZwqUY2AH7/G3/RWZWXEqtIp
/gn8Oix6739cG099C6HUj8iJn0OqI3DV0+AlaY1RWcAFq08AAAADAQABAAAAgA9fSpxREr
CoZ4TEt3nTOOmKa0Pl8oS8QLfdFd1mDkxrCwTrZZeg0H1jizlDTqq+Z1pK7Dnlb+hNKiHD
5+AKZsN8pYAteQF0Zs6jzPaB6jpoeBr7L8K3rvAGFclvuCJAvO4iuiE4kZOcCpdiU6GZXz
vzUGbUhEeZLqx9SZ62lIppAAAAQFvDzXEOKv0FwJzmzsVxx6QKq496SU3ltTyMiM9fn3ve
Sl1vB9W3LZfObk6sTLpiEOXt5V1/vNIedvJVM93pJuMAAABBAPUWCOAb5ctTF/8sQadfR+
AFtn+4Zetp9uhqDJfs1ay+AeSpq3gfQCLW3/qrdB8+1mIRFNlmBhIf9eNQHpaXM7MAAABB
AOtCQoG7m06OkoI4hR8LsRlyM34Ny59oLz8nQDkebO+TVNvrXdt3c6m/VBBLn5Tep4ppt7
e1nuez+MngfoNFi/UAAAATYWttb2RAdHlqb2huc29uLXowMQ==
-----END OPENSSH PRIVATE KEY-----"""


@contextlib.contextmanager
def _format_key(key_format, key):
    if key_format == "string":
        yield key
    elif key_format == "bytes":
        yield key.encode()
    elif key_format == "filepath":
        with tempfile.NamedTemporaryFile(delete=True) as f:
            f.write(key.encode())
            f.flush()
            yield str(f.name)


@pytest.mark.parametrize("public_key_format", ["string", "bytes", "filepath"])
@pytest.mark.parametrize("private_key_format", ["string", "bytes", "filepath"])
def test_verify_existing_keypair(hub, public_key_format, private_key_format):
    """
    Pass in every possible combination of public key and private key formats and verify that they all work
    """
    with _format_key(public_key_format, SSH_PUBLIC_KEY) as ssh_public_key:
        with _format_key(private_key_format, SSH_PRIVATE_KEY) as ssh_private_key:
            if private_key_format == "filepath":
                os.chmod(ssh_private_key, 0o600)

            with hub.tool.aws.ec2.instance.key_pair.verify(
                ssh_private_key=ssh_private_key, ssh_public_key=ssh_public_key
            ) as keypair:
                public_key, private_key_file = keypair
                assert public_key == SSH_PUBLIC_KEY

                with open(private_key_file, "rb") as fh:
                    private_key = fh.read()
                    assert private_key == SSH_PRIVATE_KEY.encode()


def test_verify_no_params(hub):
    """
    This specifically tests that the format of this key is identical to the previous format
    (when we generated this key via pycryptodomex). AWS also accepts other key formats.
    """
    with hub.tool.aws.ec2.instance.key_pair.verify() as keypair:
        public_key_header = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQ"
        private_key_header = "-----BEGIN RSA PRIVATE KEY-----\nMIIE"
        private_key_contents = "IBAAKCAQEA"
        public_key, private_key_file = keypair
        assert str(public_key).startswith(public_key_header)
        with open(private_key_file) as f:
            private_key = f.read()
            assert private_key.startswith(private_key_header)
            assert private_key_contents in private_key


def test_verify_only_public(hub):
    """
    When only a public key is given, we should raise an error
    """
    with pytest.raises(ValueError):
        with hub.tool.aws.ec2.instance.key_pair.verify(ssh_public_key="mock_key"):
            ...


def test_verify_only_private(hub):
    """
    When only a private key is given, we should raise an error
    """
    with pytest.raises(ValueError):
        with hub.tool.aws.ec2.instance.key_pair.verify(ssh_private_key="mock_key"):
            ...


def test_verify_bad_private_key_permission(hub):
    """
    When a private key has the wrong permissions, it should raise an error
    """
    with _format_key("filepath", SSH_PRIVATE_KEY) as private_key:
        os.chmod(private_key, 0o777)
        with pytest.raises(PermissionError):
            with hub.tool.aws.ec2.instance.key_pair.verify(
                ssh_private_key=private_key, ssh_public_key=SSH_PUBLIC_KEY
            ):
                ...
