def test_test_state_utils_resource_id(hub, ctx):
    test_state = hub.tool.aws.test_state_utils.generate_test_state(
        enforced_state={},
        desired_state={
            "name": "test-name",
            "description": "test-description",
        },
    )
    assert test_state
    assert test_state["resource_id"]


def test_test_state_utils_none_desired(hub, ctx):
    test_state = hub.tool.aws.test_state_utils.generate_test_state(
        enforced_state={
            "name": "esm-test-name",
            "description": "esm-test-description",
            "key": "esm-value",
        },
        desired_state={
            "name": "test-name",
            "description": "test-description",
            "key": None,
        },
    )
    assert test_state
    assert test_state["resource_id"]
    assert "esm-value" == test_state["key"]
