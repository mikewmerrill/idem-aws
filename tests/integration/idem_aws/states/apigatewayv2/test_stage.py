import copy
import time
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_stage(hub, ctx, aws_apigatewayv2_route):
    stage_temp_name = "idem-test-stage-" + str(int(time.time()))
    api_id = aws_apigatewayv2_route.get("api_id")
    route_key = aws_apigatewayv2_route.get("route_key")
    auto_deploy = True
    new_auto_deploy = False
    default_route_settings = {
        "DataTraceEnabled": True,
        "DetailedMetricsEnabled": True,
        "LoggingLevel": "OFF",
        "ThrottlingBurstLimit": 100,
        "ThrottlingRateLimit": 10,
    }
    new_default_route_settings = {
        "DataTraceEnabled": False,
        "DetailedMetricsEnabled": False,
        "LoggingLevel": "OFF",
        "ThrottlingBurstLimit": 200,
        "ThrottlingRateLimit": 20,
    }
    description = "Idem integration test - " + stage_temp_name
    new_description = "Idem integration test - new - " + stage_temp_name
    route_settings = {route_key: default_route_settings}
    new_route_settings = {route_key: new_default_route_settings}
    stage_variables = {"stage_name_var": "new-" + stage_temp_name}
    new_stage_variables = {"stage_name_var": stage_temp_name}
    tags = {"idem-resource-name": stage_temp_name}
    new_tags = {"idem-resource-name-new": "new-" + stage_temp_name}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create apigatewayv2 stage with test flag
    ret = await hub.states.aws.apigatewayv2.stage.present(
        test_ctx,
        name=stage_temp_name,
        api_id=api_id,
        auto_deploy=auto_deploy,
        default_route_settings=default_route_settings,
        description=description,
        route_settings=route_settings,
        stage_variables=stage_variables,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.apigatewayv2.stage", name=stage_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert stage_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert auto_deploy == resource.get("auto_deploy")
    assert default_route_settings == resource.get("default_route_settings")
    assert description == resource.get("description")
    assert route_settings == resource.get("route_settings")
    assert stage_variables == resource.get("stage_variables")
    assert tags == resource.get("tags")

    # Create apigatewayv2 stage
    ret = await hub.states.aws.apigatewayv2.stage.present(
        ctx,
        name=stage_temp_name,
        api_id=api_id,
        auto_deploy=auto_deploy,
        default_route_settings=default_route_settings,
        description=description,
        route_settings=route_settings,
        stage_variables=stage_variables,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.apigatewayv2.stage", name=stage_temp_name
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert stage_temp_name == resource.get("name")
    assert stage_temp_name == resource.get("resource_id")
    assert api_id == resource.get("api_id")
    assert auto_deploy == resource.get("auto_deploy")
    assert default_route_settings == resource.get("default_route_settings")
    assert description == resource.get("description")
    assert route_settings == resource.get("route_settings")
    assert stage_variables == resource.get("stage_variables")
    assert tags == resource.get("tags")

    resource_id = resource.get("resource_id")

    # Verify that the created apigatewayv2 stage is present (describe)
    ret = await hub.states.aws.apigatewayv2.stage.describe(ctx)

    assert stage_temp_name in ret
    assert "aws.apigatewayv2.stage.present" in ret.get(stage_temp_name)
    resource = ret.get(stage_temp_name).get("aws.apigatewayv2.stage.present")
    resource_map = dict(ChainMap(*resource))
    assert resource_id == resource_map.get("resource_id")
    assert stage_temp_name == resource_map.get("name")
    assert auto_deploy == resource_map.get("auto_deploy")
    assert default_route_settings == resource_map.get("default_route_settings")
    assert description == resource_map.get("description")
    assert route_settings == resource_map.get("route_settings")
    assert stage_variables == resource_map.get("stage_variables")
    assert tags == resource_map.get("tags")

    # Create apigatewayv2 stage again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.apigatewayv2.stage.present(
        test_ctx,
        name=stage_temp_name,
        api_id=api_id,
        resource_id=resource_id,
        auto_deploy=auto_deploy,
        default_route_settings=default_route_settings,
        description=description,
        route_settings=route_settings,
        stage_variables=stage_variables,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.stage",
            name=stage_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create apigatewayv2 stage again with same resource_id and no change in state
    ret = await hub.states.aws.apigatewayv2.stage.present(
        ctx,
        name=stage_temp_name,
        api_id=api_id,
        resource_id=resource_id,
        auto_deploy=auto_deploy,
        default_route_settings=default_route_settings,
        description=description,
        route_settings=route_settings,
        stage_variables=stage_variables,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.stage",
            name=stage_temp_name,
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update apigatewayv2 stage with test flag
    ret = await hub.states.aws.apigatewayv2.stage.present(
        test_ctx,
        name=stage_temp_name,
        api_id=api_id,
        resource_id=resource_id,
        auto_deploy=new_auto_deploy,
        default_route_settings=new_default_route_settings,
        description=new_description,
        route_settings=new_route_settings,
        stage_variables=new_stage_variables,
        tags=new_tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.stage",
            name=stage_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        f"Would update parameters: auto_deploy,default_route_settings,description,route_settings,stage_variables"
        in ret["comment"]
    )
    assert (
        f"Would update tags: Add {list(new_tags.keys())} Remove {list(tags.keys())}"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert stage_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert new_auto_deploy == resource.get("auto_deploy")
    assert new_default_route_settings == resource.get("default_route_settings")
    assert new_description == resource.get("description")
    assert new_route_settings == resource.get("route_settings")
    assert new_stage_variables == resource.get("stage_variables")
    assert new_tags == resource.get("tags")

    # Update apigatewayv2 stage
    ret = await hub.states.aws.apigatewayv2.stage.present(
        ctx,
        name=stage_temp_name,
        api_id=api_id,
        resource_id=resource_id,
        auto_deploy=new_auto_deploy,
        default_route_settings=new_default_route_settings,
        description=new_description,
        route_settings=new_route_settings,
        stage_variables=new_stage_variables,
        tags=new_tags,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.apigatewayv2.stage",
            name=stage_temp_name,
        )[0]
        in ret["comment"]
    )
    assert (
        f"Updated parameters: auto_deploy,default_route_settings,description,route_settings,stage_variables"
        in ret["comment"]
    )
    assert (
        f"Updated tags: Add {list(new_tags.keys())} Remove {list(tags.keys())}"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert stage_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert new_auto_deploy == resource.get("auto_deploy")
    assert new_default_route_settings == resource.get("default_route_settings")
    assert new_description == resource.get("description")
    assert new_route_settings == resource.get("route_settings")
    assert new_stage_variables == resource.get("stage_variables")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")

    # Delete apigatewayv2 stage with test flag
    ret = await hub.states.aws.apigatewayv2.stage.absent(
        test_ctx,
        name=stage_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.apigatewayv2.stage", name=stage_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert stage_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert new_auto_deploy == resource.get("auto_deploy")
    assert new_default_route_settings == resource.get("default_route_settings")
    assert new_description == resource.get("description")
    assert new_route_settings == resource.get("route_settings")
    assert new_stage_variables == resource.get("stage_variables")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")

    # Delete apigatewayv2 stage
    ret = await hub.states.aws.apigatewayv2.stage.absent(
        ctx,
        name=stage_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.apigatewayv2.stage", name=stage_temp_name
        )[0]
        in ret["comment"]
    )
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert stage_temp_name == resource.get("name")
    assert api_id == resource.get("api_id")
    assert new_auto_deploy == resource.get("auto_deploy")
    assert new_default_route_settings == resource.get("default_route_settings")
    assert new_description == resource.get("description")
    assert new_route_settings == resource.get("route_settings")
    assert new_stage_variables == resource.get("stage_variables")
    if hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigatewayv2 tags
        assert tags == resource.get("tags")
    else:
        assert new_tags == resource.get("tags")

    # Delete the same apigatewayv2 stage again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.apigatewayv2.stage.absent(
        ctx,
        name=stage_temp_name,
        api_id=api_id,
        resource_id=resource_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.stage", name=stage_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])

    # Delete apigatewayv2 stage with no resource_id will consider it as absent
    ret = await hub.states.aws.apigatewayv2.stage.absent(
        ctx, name=stage_temp_name, api_id=api_id
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigatewayv2.stage", name=stage_temp_name
        )[0]
        in ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
