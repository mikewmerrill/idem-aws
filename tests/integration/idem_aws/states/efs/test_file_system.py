import json
import tempfile
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(
    False,
    "Localstack currently has limited support for EFS file system. tests added to run with actual AWS services",
)
@pytest.mark.asyncio
async def test_file_system(hub, ctx, idem_cli, acct_profile):
    # Skip tests for localstack
    if hub.tool.utils.is_running_localstack(ctx):
        return

    creation_token = str(uuid.uuid4())
    file_system_name = "idem-test-fs-" + creation_token
    performance_mode = "generalPurpose"
    throughput_mode = "bursting"
    provisioned_throughput_in_mibps = 500.0
    availability_zone_name = ctx["acct"].get("region_name") + "b"

    resource_id = None
    try:
        # file_system goes through reconciliation, so the tests needs to be executed via idem CLI.
        with tempfile.NamedTemporaryFile(
            mode="wb+", prefix="file_system_create_", suffix=".sls", delete=True
        ) as file_system_create_sls:
            create_file_system_content = """
                        my_file_system:
                          aws.efs.file_system.present:
                            - name: {}
                            - creation_token: {}
                            - performance_mode: {}
                            - encrypted: {}
                            - throughput_mode: {}
                            - availability_zone_name: {}
                            - backup: {}
                            - provisioned_throughput_in_mibps: {}
                            - tags:
                                Name: {}
                        """.format(
                file_system_name,
                creation_token,
                performance_mode,
                False,
                throughput_mode,
                availability_zone_name,
                False,
                provisioned_throughput_in_mibps,
                file_system_name,
            )

            file_system_create_sls.write(create_file_system_content.encode())
            file_system_create_sls.flush()

            # --test
            ret = idem_cli(
                "state",
                file_system_create_sls.name,
                "--reconciler=basic",
                "--output=json",
                f"--acct-profile={acct_profile}",
                "--test",
            )

            assert ret.result is True
            output = json.loads(ret.stdout)
            assert output
            file_system_ret = next(
                v for k, v in output.items() if "aws.efs.file_system" in k
            )
            assert file_system_ret
            assert file_system_ret.get("new_state")
            assert (
                f"Would create aws.efs.file_system '{file_system_name}'"
                in file_system_ret["comment"]
            )

            # create
            ret = idem_cli(
                "state",
                file_system_create_sls.name,
                "--reconciler=basic",
                "--output=json",
                f"--acct-profile={acct_profile}",
            )

            assert ret.result is True
            output = json.loads(ret.stdout)
            assert output

            file_system_ret = next(
                v for k, v in output.items() if "aws.efs.file_system" in k
            )
            assert file_system_ret
            assert file_system_ret.get("new_state")
            resource = file_system_ret.get("new_state")

            resource_id = resource.get("resource_id", None)
            assert resource_id, file_system_ret.get("comment", "")

            # attributes
            assert file_system_name == resource.get("name")
            assert creation_token == resource.get("creation_token")
            assert performance_mode == resource["performance_mode"]
            assert not resource["encrypted"]
            assert throughput_mode == resource["throughput_mode"]
            assert availability_zone_name == resource["availability_zone_name"]
            assert not resource["backup"]
            assert resource.get("tags")
            assert "Name" in resource.get("tags")
            assert file_system_name == resource.get("tags", {}).get("Name")

            # Describe file system
            ret = await hub.states.aws.efs.file_system.describe(ctx)
            resource = ret.get(resource_id).get("aws.efs.file_system.present")
            resource_map = dict(ChainMap(*resource))
            assert creation_token == resource_map.get("creation_token")
            assert performance_mode == resource_map.get("performance_mode")
            assert not resource_map.get("encrypted")
            assert throughput_mode == resource_map.get("throughput_mode")
            assert availability_zone_name == resource_map.get("availability_zone_name")
            assert not resource_map.get("backup")
            assert resource_map.get("tags")
            assert "Name" in resource_map.get("tags")
            assert file_system_name == resource_map.get("tags", {}).get("Name")

        assert (
            resource_id
        ), f"The resource {file_system_name} might not have been created successfully."

        with tempfile.NamedTemporaryFile(
            mode="wb+", prefix="file_system_update_", suffix=".sls", delete=True
        ) as file_system_update_sls:
            updated_backup = True
            updated_throughput_mode = "provisioned"
            update_file_system_content = """
                        my_file_system:
                          aws.efs.file_system.present:
                            - name: {}
                            - resource_id: {}
                            - creation_token: {}
                            - performance_mode: {}
                            - encrypted: {}
                            - throughput_mode: {}
                            - availability_zone_name: {}
                            - backup: {}
                            - provisioned_throughput_in_mibps: {}
                            - tags:
                                Name: {}
                                id: {}
                        """.format(
                file_system_name,
                resource_id,
                creation_token,
                performance_mode,
                False,
                updated_throughput_mode,
                availability_zone_name,
                updated_backup,
                provisioned_throughput_in_mibps,
                file_system_name,
                resource_id,
            )

            file_system_update_sls.write(update_file_system_content.encode())
            file_system_update_sls.flush()

            # --test
            ret = idem_cli(
                "state",
                file_system_update_sls.name,
                "--reconciler=basic",
                "--output=json",
                f"--acct-profile={acct_profile}",
                "--test",
            )

            assert ret.result is True
            output = json.loads(ret.stdout)
            assert output
            file_system_ret = next(
                v for k, v in output.items() if "aws.efs.file_system" in k
            )
            assert file_system_ret
            assert file_system_ret.get("new_state")
            assert (
                f"Would update aws.efs.file_system '{file_system_name}'"
                in file_system_ret["comment"]
            )

            # update
            ret = idem_cli(
                "state",
                file_system_update_sls.name,
                "--reconciler=basic",
                "--output=json",
                f"--acct-profile={acct_profile}",
            )

            assert ret.result is True
            output = json.loads(ret.stdout)
            assert output

            file_system_ret = next(
                v for k, v in output.items() if "aws.efs.file_system" in k
            )
            assert file_system_ret
            assert file_system_ret.get("new_state")
            resource = file_system_ret.get("new_state")

            assert resource_id == resource.get("resource_id", None)

            assert file_system_ret["result"], file_system_ret["comment"]
            assert file_system_ret.get("old_state") and file_system_ret.get("new_state")
            assert (
                f"Updated ThroughputMode for aws.efs.file_system '{file_system_name}'"
                in file_system_ret["comment"]
            )
            assert (
                f"Updated Backup policy for aws.efs.file_system '{file_system_name}'"
                in file_system_ret["comment"]
            )

            resource = file_system_ret.get("new_state")
            assert creation_token == resource["creation_token"]
            assert performance_mode == resource["performance_mode"]
            assert updated_throughput_mode == resource["throughput_mode"]
            assert not resource["encrypted"]
            assert availability_zone_name == resource["availability_zone_name"]
            assert resource["backup"]
            assert resource.get("tags")
            assert "Name" in resource.get("tags")
            assert file_system_name == resource.get("tags", {}).get("Name")
            assert "id" in resource.get("tags")
            assert resource_id == resource.get("tags", {}).get("id")

        assert (
            resource_id
        ), f"The resource {file_system_name} might not have been created successfully."

        # Delete non-existent file system
        ret = await hub.states.aws.efs.file_system.absent(
            ctx, name="dummy", resource_id="dummy"
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("old_state") and not ret.get("new_state")
        assert f"aws.efs.file_system 'dummy' already absent" in ret["comment"]

        # Delete file system
        ret = await hub.states.aws.efs.file_system.absent(
            ctx, name=file_system_name, resource_id=resource_id
        )
        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")
        assert ret["comment"] == (f"Deleted aws.efs.file_system '{file_system_name}'",)

        # This has been deleted now
        resource_id = None
    finally:
        if resource_id:
            # Always cleanup, if there is any failure post creation
            ret = await hub.states.aws.efs.file_system.absent(
                ctx, name=file_system_name, resource_id=resource_id
            )
            assert ret["result"], ret["comment"]
