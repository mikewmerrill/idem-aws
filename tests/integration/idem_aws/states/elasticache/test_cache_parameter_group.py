import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_cache_parameter_group(hub, ctx):
    # Create, describe and delete APIs for Elasticache Parameter Group are not supported by localstack
    if hub.tool.utils.is_running_localstack(ctx):
        return

    # Create cache parameter group
    cache_parameter_group_name = "idem-test-cache-parameter-group-" + str(uuid.uuid4())
    cache_parameter_group_family = "memcached1.4"
    cache_parameter_group_description = "idem-test-cache-parameter-group-description"
    tags = {"Name": cache_parameter_group_name}
    parameter_name_values = [{"ParameterName": "chunk_size", "ParameterValue": "48"}]

    # Create elasticache parameter group with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.elasticache.cache_parameter_group.present(
        test_ctx,
        name=cache_parameter_group_name,
        cache_parameter_group_family=cache_parameter_group_family,
        description=cache_parameter_group_description,
        parameter_name_values=parameter_name_values,
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Would create aws.elasticache.cache_parameter_group '{cache_parameter_group_name}'"
        in ret["comment"]
    )
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert cache_parameter_group_name == resource.get("name")
    assert cache_parameter_group_family == resource.get("cache_parameter_group_family")
    assert cache_parameter_group_description == resource.get("description")
    assert tags == resource.get("tags")

    # Create real elasticache parameter group
    ret = await hub.states.aws.elasticache.cache_parameter_group.present(
        ctx,
        name=cache_parameter_group_name,
        cache_parameter_group_family=cache_parameter_group_family,
        description=cache_parameter_group_description,
        parameter_name_values=parameter_name_values,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert not ret["old_state"] and ret["new_state"]
    resource = ret["new_state"]
    assert cache_parameter_group_name == resource.get("name")
    assert cache_parameter_group_family == resource.get("cache_parameter_group_family")
    assert cache_parameter_group_description == resource.get("description")
    assert tags == resource.get("tags")
    resource_id = resource.get("resource_id")
    new_parameters = ret["new_state"]["parameter_name_values"]
    new_parameter_map = {
        new_param.get("ParameterName"): new_param for new_param in new_parameters
    }

    for param in parameter_name_values:
        assert param.get("ParameterName") in new_parameter_map
        assert param.get("ParameterValue") == new_parameter_map.get(
            param.get("ParameterName")
        ).get("ParameterValue")

    # Describe the created cache parameter group
    describe_ret = await hub.states.aws.elasticache.cache_parameter_group.describe(ctx)
    assert resource_id in describe_ret

    # Verify that the describe output format is correct
    assert "aws.elasticache.cache_parameter_group.present" in describe_ret.get(
        resource_id
    )
    described_resource = describe_ret.get(resource_id).get(
        "aws.elasticache.cache_parameter_group.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert cache_parameter_group_name == described_resource_map.get("name")
    assert cache_parameter_group_family == described_resource_map.get(
        "cache_parameter_group_family"
    )
    assert cache_parameter_group_description == described_resource_map.get(
        "description"
    )
    assert tags == described_resource_map.get("tags")
    new_tags = {"class": "Updated"}
    parameter_name_values = [
        {"ParameterName": "binding_protocol", "ParameterValue": "ascii"}
    ]

    # Update tags and parameter name values with test flag
    ret = await hub.states.aws.elasticache.cache_parameter_group.present(
        test_ctx,
        name=cache_parameter_group_name,
        cache_parameter_group_family=cache_parameter_group_family,
        description=cache_parameter_group_description,
        resource_id=resource_id,
        parameter_name_values=parameter_name_values,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert new_tags == ret["new_state"]["tags"]
    assert (
        f"Would update tags for aws.elasticache.cache_parameter_group {cache_parameter_group_name}"
        in ret["comment"]
    )
    new_parameters = ret["new_state"]["parameter_name_values"]
    new_parameter_map = {
        new_param.get("ParameterName"): new_param for new_param in new_parameters
    }

    for param in parameter_name_values:
        assert param.get("ParameterValue") == new_parameter_map.get(
            param.get("ParameterName")
        ).get("ParameterValue")

    # Update tags and parameter name values
    # Since modify API modifies 20 parameters at a time
    # Adding more than 20 parameters to update
    parameter_name_values = [
        {"ParameterName": "backlog_queue_limit", "ParameterValue": "1024"},
        {"ParameterName": "binding_protocol", "ParameterValue": "auto"},
        {"ParameterName": "cas_disabled", "ParameterValue": "0"},
        {"ParameterName": "chunk_size", "ParameterValue": "48"},
        {"ParameterName": "chunk_size_growth_factor", "ParameterValue": "1.25"},
        {"ParameterName": "config_max", "ParameterValue": "16"},
        {"ParameterName": "config_size_max", "ParameterValue": "65536"},
        {"ParameterName": "disable_flush_all", "ParameterValue": "0"},
        {"ParameterName": "error_on_memory_exhausted", "ParameterValue": "0"},
        {"ParameterName": "expirezero_does_not_evict", "ParameterValue": "0"},
        {"ParameterName": "hash_algorithm", "ParameterValue": "jenkins"},
        {"ParameterName": "hashpower_init", "ParameterValue": "16"},
        {"ParameterName": "idle_timeout", "ParameterValue": "0"},
        {"ParameterName": "large_memory_pages", "ParameterValue": "0"},
        {"ParameterName": "lock_down_paged_memory", "ParameterValue": "0"},
        {"ParameterName": "lru_crawler", "ParameterValue": "0"},
        {"ParameterName": "lru_maintainer", "ParameterValue": "0"},
        {"ParameterName": "maxconns_fast", "ParameterValue": "0"},
        {"ParameterName": "maximize_core_file_limit", "ParameterValue": "0"},
        {"ParameterName": "max_item_size", "ParameterValue": "1048576"},
        {"ParameterName": "max_simultaneous_connections", "ParameterValue": "65000"},
        {"ParameterName": "memcached_connections_overhead", "ParameterValue": "100"},
        {"ParameterName": "modern", "ParameterValue": "1"},
        {"ParameterName": "requests_per_event", "ParameterValue": "20"},
        {"ParameterName": "slab_automove", "ParameterValue": "0"},
        {"ParameterName": "slab_chunk_max", "ParameterValue": "524288"},
        {"ParameterName": "slab_reassign", "ParameterValue": "0"},
        {"ParameterName": "track_sizes", "ParameterValue": "0"},
        {"ParameterName": "watcher_logbuf_size", "ParameterValue": "256"},
        {"ParameterName": "worker_logbuf_size", "ParameterValue": "64"},
    ]
    ret = await hub.states.aws.elasticache.cache_parameter_group.present(
        ctx,
        name=cache_parameter_group_name,
        cache_parameter_group_family=cache_parameter_group_family,
        description=cache_parameter_group_description,
        resource_id=resource_id,
        parameter_name_values=parameter_name_values,
        tags=new_tags,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert new_tags == ret["new_state"]["tags"]
    new_parameters = ret["new_state"]["parameter_name_values"]
    new_parameter_map = {
        new_param.get("ParameterName"): new_param for new_param in new_parameters
    }

    for param in parameter_name_values:
        assert param.get("ParameterValue") == new_parameter_map.get(
            param.get("ParameterName")
        ).get("ParameterValue")

    # Remove tags
    ret = await hub.states.aws.elasticache.cache_parameter_group.present(
        ctx,
        name=cache_parameter_group_name,
        cache_parameter_group_family=cache_parameter_group_family,
        description=resource["description"],
        resource_id=cache_parameter_group_name,
        parameter_name_values=parameter_name_values,
        tags=[],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert new_tags == ret["old_state"]["tags"]
    assert not ret["new_state"].get("tags")
    assert (
        f"Updated aws.elasticache.cache_parameter_group '{cache_parameter_group_name}'"
        in ret["comment"]
    )

    # Delete cache parameter group with test flag
    ret = await hub.states.aws.elasticache.cache_parameter_group.absent(
        test_ctx, name=resource_id, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Would delete aws.elasticache.cache_parameter_group '{resource_id}'"
        in ret["comment"]
    )

    # Delete cache parameter group
    ret = await hub.states.aws.elasticache.cache_parameter_group.absent(
        ctx, name=resource_id, resource_id=resource_id
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]

    # Should not try to delete already deleted or non-existent resource.
    # It should promptly say resource is already absent
    ret = await hub.states.aws.elasticache.cache_parameter_group.absent(
        ctx, name=resource_id, resource_id=resource_id
    )
    assert (
        f"aws.elasticache.cache_parameter_group '{cache_parameter_group_name}' already absent"
        in ret["comment"]
    )
    assert not ret["old_state"] and not ret["new_state"]
