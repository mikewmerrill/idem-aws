from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "test_create_members",
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_present(hub, ctx, __test, cleanup, aws_guardduty_detector):
    global PARAMETER
    ctx["test"] = __test
    account_id = "746014882121"
    detector_id = aws_guardduty_detector.get("resource_id")
    PARAMETER["account_id"] = account_id
    PARAMETER["email"] = "xyz@vmware.com"
    PARAMETER["detector_id"] = detector_id

    present_ret = await hub.states.aws.guardduty.member.present(
        ctx,
        **PARAMETER,
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.guardduty.member",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.guardduty.member",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
        assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert not present_ret["old_state"] and present_ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["account_id"] == resource.get("account_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="already_present")
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_already_present_without_resource_id(hub, ctx, cleanup):
    global PARAMETER
    present_ret = await hub.states.aws.guardduty.member.present(
        ctx,
        **PARAMETER,
    )
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    assert (
        hub.tool.aws.comment_utils.already_exists_comment(
            resource_type="aws.guardduty.member",
            name=PARAMETER["name"],
        )[0]
        in present_ret["comment"]
    )
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["account_id"] == resource.get("account_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="already_present_diff_acc_id")
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_already_present_without_resource_id_different_account_id(
    hub,
    ctx,
    cleanup,
):
    global PARAMETER
    account_id = "dummy_account_id"
    PARAMETER["account_id"] = account_id
    account_id = PARAMETER["account_id"]
    resource_id = PARAMETER["resource_id"]
    extracted_account_id = resource_id.split(":")[1]
    present_ret = await hub.states.aws.guardduty.member.present(
        ctx,
        **PARAMETER,
    )
    assert not present_ret["result"], present_ret["comment"]
    assert (
        f"Account_id {extracted_account_id} from resource_id and account_id {account_id} passed as arg do not match."
        in present_ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="already_present_diff_detector_id")
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_already_present_without_resource_id_different_detector_id(
    hub,
    ctx,
    cleanup,
):
    global PARAMETER
    detector_id = "dummy_detector_id"
    PARAMETER["detector_id"] = detector_id
    resource_id = PARAMETER["resource_id"]
    extracted_detector_id = resource_id.split(":")[0]
    present_ret = await hub.states.aws.guardduty.member.present(
        ctx,
        **PARAMETER,
    )
    assert not present_ret["result"], present_ret["comment"]
    assert (
        f"Detector_id {extracted_detector_id} from resource_id and detector_id {detector_id} passed as arg do not match."
        in present_ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.guardduty.member.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    resource_key = f"aws-guardduty-member-{resource_id}"
    assert resource_key in describe_ret
    assert "aws.guardduty.member.present" in describe_ret[resource_key]
    described_resource = describe_ret[resource_key].get("aws.guardduty.member.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["present"])
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_exec_get(hub, ctx, aws_guardduty_detector):
    # setting to original values
    PARAMETER["detector_id"] = aws_guardduty_detector.get("resource_id")
    PARAMETER["account_id"] = "746014882121"
    ret = await hub.exec.aws.guardduty.member.get(
        ctx,
        detector_id=PARAMETER["detector_id"],
        account_id=PARAMETER["account_id"],
        resource_id=PARAMETER["resource_id"],
        name=PARAMETER["name"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["account_id"] == resource.get("account_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-list", depends=["present"])
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_exec_list(hub, ctx):
    detector_id = PARAMETER["resource_id"].split(":")[0]
    ret = await hub.exec.aws.guardduty.member.list(
        ctx, detector_id=detector_id, name=PARAMETER["name"]
    )
    assert ret["result"], ret["comment"]
    list_response = ret["ret"]
    assert list_response
    assert PARAMETER["account_id"] in str(list_response)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent")
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    resource_id = PARAMETER["resource_id"]
    absent_ret = await hub.states.aws.guardduty.member.absent(
        ctx,
        name=PARAMETER["name"],
        account_id=PARAMETER["account_id"],
        detector_id=PARAMETER["detector_id"],
        resource_id=resource_id,
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert absent_ret["old_state"] and not absent_ret["new_state"]
    resource = absent_ret["old_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.guardduty.member",
                name=PARAMETER["name"],
            )[0]
            in absent_ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.guardduty.member",
                name=PARAMETER["name"],
            )[0]
            in absent_ret["comment"]
        )
    assert resource_id == resource.get("resource_id")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["account_id"] == resource.get("account_id")
    assert PARAMETER["detector_id"] == resource.get("detector_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-not-found", depends=["absent"])
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_get_resource_id_does_not_exist(hub, ctx):
    # pass a non-existent id
    ret = await hub.exec.aws.guardduty.member.get(
        ctx,
        detector_id=PARAMETER["detector_id"],
        account_id=PARAMETER["account_id"],
        resource_id=PARAMETER["resource_id"],
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert (
        hub.tool.aws.comment_utils.get_empty_comment(
            resource_type="aws.guardduty.member", name=PARAMETER["name"]
        )
        in ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    resource_id = PARAMETER["resource_id"]
    absent_ret = await hub.states.aws.guardduty.member.absent(
        ctx,
        name=PARAMETER["name"],
        detector_id=PARAMETER["detector_id"],
        account_id=PARAMETER["account_id"],
        resource_id=resource_id,
    )
    assert absent_ret["result"], absent_ret["comment"]
    assert (not absent_ret["old_state"]) and (not absent_ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.guardduty.member",
            name=PARAMETER["name"],
        )[0]
        in absent_ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Skipping test in localstack as guardduty is not supported in localstack.",
)
async def test_member_absent_with_none_resource_id(hub, ctx):
    ret = await hub.states.aws.guardduty.member.absent(
        ctx,
        name=PARAMETER["name"],
        detector_id=PARAMETER["detector_id"],
        account_id=PARAMETER["account_id"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.guardduty.member",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if not hub.tool.utils.is_running_localstack(ctx):
        if "resource_id" in PARAMETER:
            ret = await hub.states.aws.guardduty.member.absent(
                ctx,
                name=PARAMETER["name"],
                resource_id=PARAMETER["resource_id"],
                detector_id=PARAMETER["detector_id"],
                account_id=PARAMETER["account_id"],
            )
            assert ret["result"], ret["comment"]
