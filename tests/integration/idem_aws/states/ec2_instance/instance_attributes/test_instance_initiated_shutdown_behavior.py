import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_update_instance_initiated_shutdown_behavior(
    hub, ctx, aws_ec2_instance, __test
):
    state = copy.copy(aws_ec2_instance)
    state["instance_initiated_shutdown_behavior"] = "terminate"

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert (
            ret["changes"]["new"]["instance_initiated_shutdown_behavior"] == "terminate"
        )
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
async def test_reset_instance_initiated_shutdown_behavior(
    hub, ctx, aws_ec2_instance, aws_iam_role, __test
):
    state = copy.copy(aws_ec2_instance)
    state["instance_initiated_shutdown_behavior"] = None

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert (
            not ret["changes"]
            .get("new", {})
            .get("instance_initiated_shutdown_behavior")
        )
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
