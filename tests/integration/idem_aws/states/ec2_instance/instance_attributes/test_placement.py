import copy

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update_group_name")
async def test_update_group_name(
    hub, ctx, aws_ec2_instance, aws_ec2_placement_group, __test
):
    state = copy.copy(aws_ec2_instance)
    group_name = aws_ec2_placement_group["resource_id"]
    state["group_name"] = group_name

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not ret["changes"]["old"].get("group_name")
        assert ret["changes"]["new"]["group_name"] == group_name
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["update_group_name"])
async def test_reset_group_name(hub, ctx, aws_ec2_instance, __test):
    state = copy.copy(aws_ec2_instance)
    state["group_name"] = ""

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert not ret["changes"].get("new").get("group_name")
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
async def test_update_affinity_and_tenancy(hub, ctx, aws_ec2_instance, __test):
    return
    # TODO A dedicated host needs to be allocated for this instance type before we can test this
    state = copy.deepcopy(aws_ec2_instance)
    state["tenancy"] = "host"
    state["affinity"] = "host"

    # Run the present state with a modified value
    ret = await hub.states.aws.ec2.instance.present(ctx, **state)
    assert ret["result"], "\n".join(str(x) for x in ret["comment"])

    # Before creation, report changes that would be made
    if __test <= 1:
        assert ret["changes"]["old"]["tenancy"] == "default"
        assert ret["changes"]["new"]["tenancy"] == "host"
        assert not ret["changes"]["old"].get("affinity")
        assert ret["changes"]["new"]["affinity"] == "host"
    # After creation, no changes made
    else:
        assert not ret["changes"] is None, ret["changes"]
