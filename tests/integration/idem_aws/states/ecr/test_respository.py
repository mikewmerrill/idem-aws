import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.localstack(False, "LocalStack does not have support for ECR repositories")
@pytest.mark.asyncio
async def test_repository(hub, ctx, aws_test_ecr_repository):
    resource_id = aws_test_ecr_repository
    registry_id = None  # TODO: when we implement support for ecr registries, we must create one dynamically at the conftest
    image_tag_mutability_immutable = "IMMUTABLE"
    image_tag_mutability_mutable = "MUTABLE"
    image_scanning_configuration_scan_on_push_true = {"scanOnPush": True}
    image_scanning_configuration_scan_on_push_false = {"scanOnPush": True}
    encryption_configuration_type_aes256 = {"encryptionType": "AES256"}
    encryption_configuration_type_kms = {"encryptionType": "KMS"}
    tags = {"Name": aws_test_ecr_repository}
    new_tags = {"NewName": aws_test_ecr_repository}

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Create ecr repository with test flag
    ret = await hub.states.aws.ecr.repository.present(
        test_ctx,
        name=aws_test_ecr_repository,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_immutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_true,
        encryption_configuration=encryption_configuration_type_aes256,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        hub.tool.aws.comment_utils.would_create_comment(
            resource_type="aws.ecr.repository", name=aws_test_ecr_repository
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert aws_test_ecr_repository == resource.get("name")
    assert resource_id == resource.get("resource_id")
    # assert registry_id == resource.get("registryId")
    assert aws_test_ecr_repository == resource.get("repository_name")
    assert image_tag_mutability_immutable == resource.get("image_tag_mutability")
    assert image_scanning_configuration_scan_on_push_true == resource.get(
        "image_scanning_configuration"
    )
    assert encryption_configuration_type_aes256 == resource.get(
        "encryption_configuration"
    )
    assert tags == resource.get("tags")

    # Create ecr repository
    ret = await hub.states.aws.ecr.repository.present(
        ctx,
        name=aws_test_ecr_repository,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_immutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_true,
        encryption_configuration=encryption_configuration_type_aes256,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        hub.tool.aws.comment_utils.create_comment(
            resource_type="aws.ecr.repository", name=aws_test_ecr_repository
        )[0]
        in ret["comment"]
    )
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert aws_test_ecr_repository == resource.get("name")
    assert resource_id == resource.get("resource_id")
    # assert registry_id == resource.get("registry_id")
    assert aws_test_ecr_repository == resource.get("repository_name")
    assert image_tag_mutability_immutable == resource.get("image_tag_mutability")
    assert image_scanning_configuration_scan_on_push_true == resource.get(
        "image_scanning_configuration"
    )
    assert encryption_configuration_type_aes256 == resource.get(
        "encryption_configuration"
    )
    assert tags == resource.get("tags")

    # Verify that the created ecr repository is present (describe)
    ret = await hub.states.aws.ecr.repository.describe(ctx)

    assert aws_test_ecr_repository in ret
    assert "aws.ecr.repository.present" in ret.get(resource_id)
    resource = ret.get(resource_id).get("aws.ecr.repository.present")
    resource_map = dict(ChainMap(*resource))
    assert aws_test_ecr_repository == resource_map.get("name")
    assert resource_id == resource_map.get("resource_id")
    # assert registry_id == resource_map.get("registry_id")
    assert aws_test_ecr_repository == resource_map.get("repository_name")
    assert image_tag_mutability_immutable == resource_map.get("image_tag_mutability")
    assert image_scanning_configuration_scan_on_push_true == resource_map.get(
        "image_scanning_configuration"
    )
    assert encryption_configuration_type_aes256 == resource_map.get(
        "encryption_configuration"
    )
    assert tags == resource_map.get("tags")

    # Create ecr repository again with same resource_id and no change in state with test flag
    ret = await hub.states.aws.ecr.repository.present(
        test_ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_immutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_true,
        encryption_configuration=encryption_configuration_type_aes256,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        f"aws.ecr.repository '{aws_test_ecr_repository}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Create ecr repository again with same resource_id and no change in state
    ret = await hub.states.aws.ecr.repository.present(
        ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_immutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_true,
        encryption_configuration=encryption_configuration_type_aes256,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        f"aws.ecr.repository '{aws_test_ecr_repository}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update ecr repository with test flag (not supported)
    ret = await hub.states.aws.ecr.repository.present(
        test_ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_mutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_false,
        encryption_configuration=encryption_configuration_type_kms,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        f"aws.ecr.repository '{aws_test_ecr_repository}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update ecr repository (not supported)
    ret = await hub.states.aws.ecr.repository.present(
        ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_mutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_false,
        encryption_configuration=encryption_configuration_type_kms,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        f"aws.ecr.repository '{aws_test_ecr_repository}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")

    # Update tags from ecr repository with test flag
    ret = await hub.states.aws.ecr.repository.present(
        test_ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_immutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_true,
        encryption_configuration=encryption_configuration_type_aes256,
        tags=new_tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        f"aws.ecr.repository '{aws_test_ecr_repository}' already exists"
        in ret["comment"]
    )
    assert any("Would update tags:" in comment for comment in ret["comment"])
    assert (
        f"Would have updated tags for aws.ecr.repository '{aws_test_ecr_repository}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")

    # Update tags from ecr repository
    ret = await hub.states.aws.ecr.repository.present(
        ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_immutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_true,
        encryption_configuration=encryption_configuration_type_aes256,
        tags=new_tags,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        f"aws.ecr.repository '{aws_test_ecr_repository}' already exists"
        in ret["comment"]
    )
    assert any("Update tags:" in comment for comment in ret["comment"])
    assert f"Updated aws.ecr.repository '{aws_test_ecr_repository}'" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert new_tags == resource.get("tags")

    # Remove tags from ecr repository with test flag
    ret = await hub.states.aws.ecr.repository.present(
        test_ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_immutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_true,
        encryption_configuration=encryption_configuration_type_aes256,
        tags=[],
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        f"aws.ecr.repository '{aws_test_ecr_repository}' already exists"
        in ret["comment"]
    )
    assert any("Would update tags:" in comment for comment in ret["comment"])
    assert (
        f"Would have updated tags for aws.ecr.repository '{aws_test_ecr_repository}'"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("tags") is None

    # Remove tags from ecr repository
    ret = await hub.states.aws.ecr.repository.present(
        ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
        image_tag_mutability=image_tag_mutability_immutable,
        image_scanning_configuration=image_scanning_configuration_scan_on_push_true,
        encryption_configuration=encryption_configuration_type_aes256,
        tags=[],
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        f"aws.ecr.repository '{aws_test_ecr_repository}' already exists"
        in ret["comment"]
    )
    assert any("Update tags:" in comment for comment in ret["comment"])
    assert f"Updated aws.ecr.repository '{aws_test_ecr_repository}'" in ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("tags") is None

    # Search for existing ecr repository
    ret = await hub.exec.aws.ecr.repository.get(
        ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    resource = ret.get("ret")
    assert aws_test_ecr_repository == resource.get("name")
    assert resource_id == resource.get("resource_id")
    # assert registry_id == resource.get("registryId")
    assert aws_test_ecr_repository == resource.get("repository_name")
    assert image_tag_mutability_immutable == resource.get("image_tag_mutability")
    assert image_scanning_configuration_scan_on_push_true == resource.get(
        "image_scanning_configuration"
    )
    assert encryption_configuration_type_aes256 == resource.get(
        "encryption_configuration"
    )
    assert resource.get("tags") is None

    # Search for non-existing ecr repository
    fake_repository_temp_name = "fake-" + str(uuid.uuid4())
    ret = await hub.exec.aws.ecr.repository.get(
        ctx,
        name=fake_repository_temp_name,
        resource_id=fake_repository_temp_name,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        hub.tool.aws.comment_utils.get_empty_comment(
            resource_type="aws.ecr.repository", name=fake_repository_temp_name
        )
        in ret["comment"]
    )
    assert not ret.get("ret")

    # Delete ecr repository with test flag
    ret = await hub.states.aws.ecr.repository.absent(
        test_ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        hub.tool.aws.comment_utils.would_delete_comment(
            resource_type="aws.ecr.repository", name=aws_test_ecr_repository
        )[0]
        in ret["comment"]
    )

    # Delete ecr repository
    ret = await hub.states.aws.ecr.repository.absent(
        ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        hub.tool.aws.comment_utils.delete_comment(
            resource_type="aws.ecr.repository", name=aws_test_ecr_repository
        )[0]
        in ret["comment"]
    )

    # Delete the same ecr repository again (deleted state) will not invoke delete on AWS side
    ret = await hub.states.aws.ecr.repository.absent(
        ctx,
        name=aws_test_ecr_repository,
        resource_id=resource_id,
        registry_id=registry_id,
    )

    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ecr.repository", name=aws_test_ecr_repository
        )[0]
        in ret["comment"]
    )

    # Delete ecr repository with no resource_id
    ret = await hub.states.aws.ecr.repository.absent(
        ctx,
        name=aws_test_ecr_repository,
    )

    assert ret["result"], ret["comment"]
    assert ret["result"] is True
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.ecr.repository", name=aws_test_ecr_repository
        )[0]
        in ret["comment"]
    )
