"""Tests for validating Secretsmanager Secrets."""
import uuid
from collections import ChainMap

import pytest


PARAMETRIZE = {
    "argnames": "__test",
    "argvalues": [True, False],
    "ids": ["--test", "run"],
}

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test):
    r"""
    **Test function**
    """
    tags = {"Name": "idem-test-secret"}
    global PARAMETER
    ctx["test"] = __test
    # Create the resource
    ret = await hub.states.aws.secretsmanager.secret.present(
        ctx,
        name=PARAMETER["name"],
        description="idem-test-secret",
        secret_string="idem-test-secret",
        tags=tags,
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert (
            f"Would create aws.secretsmanager.secret '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Created aws.secretsmanager.secret '{PARAMETER['name']}'" in ret["comment"]
        )

    PARAMETER["resource_id"] = resource["resource_id"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")

    if not __test:
        # Now get the resource with exec
        ret = await hub.exec.aws.secretsmanager.secret.get(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"]
        resource = ret["ret"]
        assert PARAMETER["name"] == resource.get("name")
        assert "idem-test-secret" == resource["description"]
        assert tags == resource["tags"]

        tags_updated = {"Name": "idem-test-secret-updated"}

        # Now Update the resource
        ret = await hub.states.aws.secretsmanager.secret.present(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
            description="idem-test-secret-updated",
            secret_string="idem-test-secret",
            tags=tags_updated,
        )
        assert (
            f"Updated aws.secretsmanager.secret '{PARAMETER['name']}'" in ret["comment"]
        )
        assert ret["result"], ret["comment"]

        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        if not hub.tool.utils.is_running_localstack(ctx):
            assert "idem-test-secret-updated" == resource["description"]
        assert tags_updated == resource["tags"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    r"""
    **Test function**
    """

    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ret = await hub.states.aws.secretsmanager.secret.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in ret
    assert "aws.secretsmanager.secret.present" in ret[resource_id]
    described_resource = ret[resource_id].get("aws.secretsmanager.secret.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["resource_id"] == described_resource_map["resource_id"]
    # localstack not updating description
    if not hub.tool.utils.is_running_localstack(ctx):
        assert "idem-test-secret-updated" == described_resource_map["description"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    r"""
    **Test function**
    """

    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ctx["test"] = __test
    # Delete the resource
    ret = await hub.states.aws.secretsmanager.secret.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )

    if __test:
        assert (
            f"Would delete aws.secretsmanager.secret '{PARAMETER['name']}'"
            in ret["comment"]
        )
    else:
        assert (
            f"Deleted aws.secretsmanager.secret '{PARAMETER['name']}'" in ret["comment"]
        )

        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")
        ret.get("old_state")

        # Secret is not immediately deleted, There is a minimum 7 days window before secret is permanently deleted.

        # # Now get the resource with exec - Should not exist
        # ret = await hub.exec.aws.secretsmanager.secret.get(
        #     ctx,
        #     name=PARAMETER["name"],
        #     resource_id=PARAMETER["resource_id"]
        # )
        # assert ret
        # assert ret["result"], ret["comment"]
        # assert ret["ret"] is None
        # assert "result is empty" in str(ret["comment"])
