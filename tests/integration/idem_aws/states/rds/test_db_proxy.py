"""Tests for validating Rds Db Proxys."""
import uuid
from collections import ChainMap

import pytest

PARAMETRIZE = {
    "argnames": "__test",
    "argvalues": [True, False],
    "ids": ["--test", "run"],
}

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    aws_iam_role,
    aws_secretsmanager_secret,
    aws_ec2_subnet,
    aws_ec2_subnet_2,
    __test,
):
    r"""
    **Test function**
    """

    global PARAMETER
    ctx["test"] = __test

    auth = {
        "AuthScheme": "SECRETS",
        "ClientPasswordAuthType": "POSTGRES_SCRAM_SHA_256",
        "IAMAuth": "DISABLED",
        "SecretArn": aws_secretsmanager_secret.get("resource_id"),
    }

    # Create the resource
    ret = await hub.states.aws.rds.db_proxy.present(
        ctx,
        name=PARAMETER["name"],
        db_proxy_name=PARAMETER["name"],
        engine_family="POSTGRESQL",
        auth=[auth],
        role_arn=aws_iam_role.get("arn"),
        vpc_subnet_ids=[
            aws_ec2_subnet.get("SubnetId"),
            aws_ec2_subnet_2.get("SubnetId"),
        ],
        idle_client_timeout=5400,
        tags={"Name": "idem-test"},
    )
    assert ret["result"], ret["comment"]
    resource = ret["new_state"]
    if __test:
        assert f"Would create aws.rds.db_proxy '{PARAMETER['name']}'" in ret["comment"]
    else:
        assert f"Created aws.rds.db_proxy '{PARAMETER['name']}'" in ret["comment"]

    PARAMETER["resource_id"] = resource["resource_id"]
    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == resource.get("name")

    if not __test:
        # Now get the resource with exec
        ret = await hub.exec.aws.rds.db_proxy.get(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"]
        resource = ret["ret"]
        assert PARAMETER["name"] == resource.get("name")

        # Now Update the resource
        ret = await hub.states.aws.rds.db_proxy.present(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
            db_proxy_name=PARAMETER["name"],
            engine_family="POSTGRESQL",
            auth=[auth],
            role_arn=aws_iam_role.get("arn"),
            vpc_subnet_ids=[
                aws_ec2_subnet.get("SubnetId"),
                aws_ec2_subnet_2.get("SubnetId"),
            ],
            idle_client_timeout=5200,
            tags={"Name": "idem-test1"},
        )
        assert f"Updated aws.rds.db_proxy '{PARAMETER['name']}'" in ret["comment"]
        assert ret["result"], ret["comment"]

        assert ret.get("old_state") and ret.get("new_state")
        resource = ret["new_state"]
        assert {"Name": "idem-test1"} == resource["tags"]
        assert 5200 == resource["idle_client_timeout"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    r"""
    **Test function**
    """

    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ret = await hub.states.aws.rds.db_proxy.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in ret
    assert "aws.rds.db_proxy.present" in ret[resource_id]
    described_resource = ret[resource_id].get("aws.rds.db_proxy.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert {"Name": "idem-test1"} == described_resource_map["tags"]
    assert 5200 == described_resource_map["idle_client_timeout"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    r"""
    **Test function**
    """

    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"
    ctx["test"] = __test
    # Delete the resource
    ret = await hub.states.aws.rds.db_proxy.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )

    if __test:
        assert f"Would delete aws.rds.db_proxy '{PARAMETER['name']}'" in ret["comment"]
    else:
        assert f"Deleted aws.rds.db_proxy '{PARAMETER['name']}'" in ret["comment"]

        assert ret["result"], ret["comment"]
        assert ret.get("old_state") and not ret.get("new_state")
        ret.get("old_state")

        # Now get the resource with exec - Should not exist
        ret = await hub.exec.aws.rds.db_proxy.get(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret
        assert ret["result"], ret["comment"]
        assert ret["ret"] is None
        assert "result is empty" in str(ret["comment"])

    if not __test:
        # Try deleting the resource again
        ret = await hub.states.aws.rds.db_proxy.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )

        assert (
            f"aws.rds.db_proxy '{PARAMETER['name']}' already absent" in ret["comment"]
        )
        assert ret["result"], ret["comment"]
        assert (not ret["old_state"]) and (not ret["new_state"])
        PARAMETER.pop("resource_id")
