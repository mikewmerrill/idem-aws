import copy
import json
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-iam-policy-" + str(int(time.time())),
    "description": "Idem IAM policy integration test",
    "path": "/",
    "policy_document": {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "AllowCreateSubnet",
                "Effect": "Allow",
                "Action": ["ec2:CreateSubnet"],
                "Resource": "*",
            }
        ],
    },
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, cleanup):
    global PARAMETER
    ctx["test"] = __test
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}

    ret = await hub.states.aws.iam.policy.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]

    resource = ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    assert not ret["old_state"] and ret["new_state"]

    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["path"] == resource.get("path")
    assert PARAMETER["policy_document"] == json.loads(resource.get("policy_document"))
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-by-resource-id", depends=["present"])
async def test_get_by_resource_id(hub, ctx):
    ret = await hub.exec.aws.iam.policy.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]

    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["path"] == resource.get("path")
    assert PARAMETER["policy_document"] == json.loads(resource.get("policy_document"))
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="exec-get-by-resource-id-not-found", depends=["exec-get-by-resource-id"]
)
async def test_get_resource_id_not_found(hub, ctx):
    fake_name = "idem-test-fake-iam-policy-" + str(int(time.time()))
    resource_id = hub.tool.aws.arn_utils.build(
        service="iam",
        account_id="123456789012",
        resource="policy/" + fake_name,
    )
    ret = await hub.exec.aws.iam.policy.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert hub.tool.aws.comment_utils.get_empty_comment(
        "aws.iam.policy",
        PARAMETER["name"],
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="exec-get-by-name", depends=["exec-get-by-resource-id-not-found"]
)
async def test_get_by_name(hub, ctx):
    ret = await hub.exec.aws.iam.policy.get(
        ctx,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]

    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["path"] == resource.get("path")
    assert PARAMETER["policy_document"] == json.loads(resource.get("policy_document"))
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-by-name-not-found", depends=["exec-get-by-name"])
async def test_get_by_name_not_found(hub, ctx):
    fake_name = "idem-test-fake-iam-policy-" + str(int(time.time()))
    ret = await hub.exec.aws.iam.policy.get(
        ctx,
        name=fake_name,
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert hub.tool.aws.comment_utils.get_empty_comment(
        "aws.iam.policy",
        fake_name,
    ) in str(ret["comment"])


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-list", depends=["exec-get-by-name-not-found"])
async def test_exec_list(hub, ctx):
    ret = await hub.exec.aws.iam.policy.list(
        ctx,
        scope="Local",
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]

    resource = list(
        filter(
            lambda policy: policy["resource_id"] == PARAMETER["resource_id"], ret["ret"]
        )
    )
    assert resource
    assert PARAMETER["name"] == resource[0].get("name")
    assert PARAMETER["description"] == resource[0].get("description")
    assert PARAMETER["path"] == resource[0].get("path")
    assert PARAMETER["policy_document"] == json.loads(
        resource[0].get("policy_document")
    )
    assert PARAMETER["tags"] == resource[0].get("tags")


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["exec-list"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.iam.policy.describe(ctx)
    resource_id = f"iam-policy-{PARAMETER['resource_id']}"
    assert resource_id in describe_ret

    assert "aws.iam.policy.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get("aws.iam.policy.present")
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["name"] == described_resource_map.get("name")
    assert PARAMETER["description"] == described_resource_map.get("description")
    assert PARAMETER["path"] == described_resource_map.get("path")
    assert PARAMETER["policy_document"] == json.loads(
        described_resource_map.get("policy_document")
    )
    assert PARAMETER["tags"] == described_resource_map.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="update_attributes", depends=["describe"])
async def test_update_attributes(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["policy_document"] = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "DenyCreateSubnet",
                "Effect": "Deny",
                "Action": ["ec2:CreateSubnet"],
                "Resource": "*",
            }
        ],
    }

    ret = await hub.states.aws.iam.policy.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["description"] == old_resource.get("description")
    assert PARAMETER["path"] == old_resource.get("path")
    assert PARAMETER["policy_document"] == json.loads(
        old_resource.get("policy_document")
    )
    assert PARAMETER["tags"] == old_resource.get("tags")

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["description"] == resource.get("description")
    assert new_parameter["path"] == resource.get("path")
    assert new_parameter["policy_document"] == json.loads(
        resource.get("policy_document")
    )
    assert new_parameter["tags"] == resource.get("tags")

    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert old_resource["default_version_id"] != resource["default_version_id"]
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="add_tags", depends=["update_attributes"])
async def test_add_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["tags"].update(
        {
            "New-Tag": f"idem-test-value-{str(int(time.time()))}",
        }
    )

    ret = await hub.states.aws.iam.policy.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["description"] == old_resource.get("description")
    assert PARAMETER["path"] == old_resource.get("path")
    assert PARAMETER["policy_document"] == json.loads(
        old_resource.get("policy_document")
    )
    assert PARAMETER["tags"] == old_resource.get("tags")

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["description"] == resource.get("description")
    assert new_parameter["path"] == resource.get("path")
    assert new_parameter["policy_document"] == json.loads(
        resource.get("policy_document")
    )
    assert new_parameter["tags"] == resource.get("tags")

    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            + hub.tool.aws.comment_utils.update_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="remove_tags", depends=["add_tags"])
async def test_remove_tags(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test

    new_parameter = copy.deepcopy(PARAMETER)
    del new_parameter["tags"]["New-Tag"]

    ret = await hub.states.aws.iam.policy.present(ctx, **new_parameter)

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource["name"]
    assert PARAMETER["resource_id"] == old_resource["resource_id"]
    assert PARAMETER["description"] == old_resource.get("description")
    assert PARAMETER["path"] == old_resource.get("path")
    assert PARAMETER["policy_document"] == json.loads(
        old_resource.get("policy_document")
    )
    assert PARAMETER["tags"] == old_resource.get("tags")

    resource = ret["new_state"]
    assert new_parameter["name"] == resource["name"]
    assert new_parameter["resource_id"] == resource["resource_id"]
    assert new_parameter["description"] == resource.get("description")
    assert new_parameter["path"] == resource.get("path")
    assert new_parameter["policy_document"] == json.loads(
        resource.get("policy_document")
    )
    assert new_parameter["tags"] == resource.get("tags")

    added = {
        tag: new_parameter["tags"][tag]
        for tag in new_parameter["tags"]
        if tag not in PARAMETER["tags"]
    }
    removed = {
        tag: PARAMETER["tags"][tag]
        for tag in PARAMETER["tags"]
        if tag not in new_parameter["tags"]
    }
    if __test:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.would_update_tags_comment(removed, added)
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.already_exists_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            + hub.tool.aws.comment_utils.update_tags_comment(removed, added)
            + hub.tool.aws.comment_utils.update_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            == ret["comment"]
        )

    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["remove_tags"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test

    ret = await hub.states.aws.iam.policy.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                "aws.iam.policy",
                PARAMETER["name"],
            )
            == ret["comment"]
        )
    assert ret["old_state"] and not ret["new_state"]

    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["description"] == old_resource.get("description")
    assert PARAMETER["path"] == old_resource.get("path")
    assert PARAMETER["policy_document"] == json.loads(
        old_resource.get("policy_document")
    )
    assert PARAMETER["tags"] == old_resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test

    ret = await hub.states.aws.iam.policy.absent(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            "aws.iam.policy",
            PARAMETER["name"],
        )
        == ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="absent_with_none_resource_id", depends=["already_absent"])
async def test_absent_with_none_resource_id(hub, ctx, __test):
    ctx["test"] = __test

    ret = await hub.states.aws.iam.policy.absent(
        ctx, name=PARAMETER["name"], resource_id=None
    )
    assert ret["result"], ret["comment"]
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            "aws.iam.policy",
            PARAMETER["name"],
        )
        == ret["comment"]
    )
    assert (not ret["old_state"]) and (not ret["new_state"])


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER

    yield None

    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.iam.policy.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]
