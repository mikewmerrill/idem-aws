import tempfile

import pytest

ACCESS_KEY_ID = "AKIAIOSFODNN7EXAMPLE"
ACCESS_SECRET_ACCESS_KEY = "wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
REGION = "us-west-2"

# ~/.aws/credentials
AWS_CREDS = f"""
[default]
aws_access_key_id: default_key
aws_secret_access_key: default_id

[test_profile]
aws_access_key_id: {ACCESS_KEY_ID}
aws_secret_access_key: {ACCESS_SECRET_ACCESS_KEY}
"""


def test_fallback_awscli_default(idem_cli):
    """
    Test the ability of AWS to use credentials from awscli when no acct file or acct key is specified
    Use the default profile from awscli
    """
    with tempfile.NamedTemporaryFile() as fh:
        # Set up a temporary aws config file
        fh.write(AWS_CREDS.encode())
        fh.flush()
        ret = idem_cli(
            "exec",
            "boto3.client.ec2.describe_vpcs",
            env={
                "AWS_SHARED_CREDENTIALS_FILE": fh.name,
                "ACCT_FILE": "",
                "ACCT_KEY": "",
            },
        )

        assert "ConnectionError: No profiles found" not in ret.stderr, "Message"

        ret = idem_cli(
            "exec",
            "aws.test.ctx",
            env={
                "AWS_SHARED_CREDENTIALS_FILE": fh.name,
                "ACCT_FILE": "",
                "ACCT_KEY": "",
            },
        )

        assert ret.result
        assert ret.json
        assert ret.json.ret
        assert ret.json.ret.aws_access_key_id == "default_key"
        assert ret.json.ret.aws_secret_access_key == "default_id"


def test_fallback_awscli_named_profile(idem_cli):
    """
    Test the ability of AWS to use credentials from awscli when a non-default profile is used
    """
    with tempfile.NamedTemporaryFile() as fh:
        # Set up a temporary aws config file
        fh.write(AWS_CREDS.encode())
        fh.flush()
        ret = idem_cli(
            "exec",
            "boto3.client.ec2.describe_vpcs",
            "--acct-profile=test_profile",
            env={
                "AWS_SHARED_CREDENTIALS_FILE": fh.name,
                "ACCT_FILE": "",
                "ACCT_KEY": "",
            },
        )

        assert "ConnectionError: No profiles found" not in ret.stderr, "Message"

        ret = idem_cli(
            "exec",
            "aws.test.ctx",
            "--acct-profile=test_profile",
            env={
                "AWS_SHARED_CREDENTIALS_FILE": fh.name,
                "ACCT_FILE": "",
                "ACCT_KEY": "",
            },
        )

        assert ret.result
        assert ret.json
        assert ret.json.ret
        assert ret.json.ret.aws_access_key_id == ACCESS_KEY_ID
        assert ret.json.ret.aws_secret_access_key == ACCESS_SECRET_ACCESS_KEY


def test_fallback_awscli_no_matching_profile(idem_cli):
    """
    Test the ability of AWS to use credentials from awscli when the named default isn't present.
    The default will be decided by AWSCLI and made to match --acct-profile
    """
    with tempfile.NamedTemporaryFile() as fh:
        # Set up a temporary aws config file
        fh.write(AWS_CREDS.encode())
        fh.flush()
        ret = idem_cli(
            "exec",
            "aws.test.ctx",
            "--acct-profile=taco",
            env={"AWS_SHARED_CREDENTIALS_FILE": fh.name, "ACCT_FILE": ""},
        )
        assert ret.result
        assert ret.json
        assert ret.json.ret
        assert ret.json.ret.aws_access_key_id == "default_key"
        assert ret.json.ret.aws_secret_access_key == "default_id"
