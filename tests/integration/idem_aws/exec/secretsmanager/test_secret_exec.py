"""Tests for validating Secretsmanager Secrets."""
import uuid

import pytest

PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.dependency(name="create")
async def test_create(hub, ctx):
    r"""
    **Test function**
    """

    tags = {"Name": "idem-test-secret"}
    # Test - create new resource
    global PARAMETER
    ret = await hub.exec.aws.secretsmanager.secret.create(
        ctx,
        name=PARAMETER["name"],
        description="idem-test-secret",
        secret_string="idem-test-secret",
        tags=tags,
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]

    # Now get the resource
    ret = await hub.exec.aws.secretsmanager.secret.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert "idem-test-secret" == resource["description"]
    assert tags == resource["tags"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="get", depends=["create"])
async def test_get(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.secretsmanager.secret.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert "idem-test-secret" == resource["description"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="list", depends=["create"])
async def test_list(hub, ctx):
    r"""
    **Test function**
    """
    global PARAMETER
    ret = await hub.exec.aws.secretsmanager.secret.list(ctx)
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource
        assert "resource_id" in resource


@pytest.mark.asyncio
@pytest.mark.dependency(name="update", depends=["list"])
async def test_update(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Update existing resource
    global PARAMETER

    tags_updated = {"Name": "idem-test-secret-updated"}

    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.secretsmanager.secret.update(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        description="idem-test-secret-updated",
        tags=tags_updated,
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource

    # Now get the resource
    ret = await hub.exec.aws.secretsmanager.secret.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert tags_updated == resource["tags"]
    if not hub.tool.utils.is_running_localstack(ctx):
        assert "idem-test-secret-updated" == resource["description"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="delete", depends=["update"])
async def test_delete(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Delete existing resource
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.secretsmanager.secret.delete(
        ctx, name="", resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Secret is not immediately deleted, There is a minimum 7 days window before secret is permanently deleted.

    # # Now get the resource
    # ret = await hub.exec.aws.secretsmanager.secret.get(
    #     ctx,
    #     name=PARAMETER["name"],
    #     resource_id=PARAMETER["resource_id"]
    # )
    # assert ret
    # assert ret["result"], ret["comment"]
    # assert ret["ret"] is None
    # assert "result is empty" in str(ret["comment"])
