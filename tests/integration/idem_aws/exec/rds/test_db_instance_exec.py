import uuid

import pytest
import pytest_asyncio

DB_INSTANCE_NAME = "idem-test-db-instance-" + str(uuid.uuid4())
PARAMETER = {
    "resource_id": DB_INSTANCE_NAME,
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_get(hub, ctx, aws_rds_db_cluster, cleanup):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER

    await _create_db_instance(hub, ctx, aws_rds_db_cluster)
    ret = await hub.exec.aws.rds.db_instance.get(
        ctx,
        **PARAMETER,
    )

    assert ret["result"]
    resource = ret["ret"]
    assert resource["resource_id"] == PARAMETER["resource_id"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_list(hub, ctx, aws_rds_db_cluster, cleanup):
    if hub.tool.utils.is_running_localstack(ctx):
        return
    global PARAMETER

    await _create_db_instance(hub, ctx, aws_rds_db_cluster)
    ret = await hub.exec.aws.rds.db_instance.list(ctx)

    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], list)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert resource["resource_id"] == PARAMETER["resource_id"]


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.rds.db_instance.absent(
            ctx,
            name=PARAMETER["resource_id"],
            resource_id=PARAMETER["resource_id"],
        )
        assert ret["result"], ret["comment"]


async def _create_db_instance(hub, ctx, aws_rds_db_cluster):
    global PARAMETER
    db_instance_class = "db.t3.small"
    engine = "aurora-mysql"
    db_subnet_group_name = aws_rds_db_cluster.get("db_subnet_group_name")
    availability_zone = ctx["acct"].get("region_name") + "a"
    preferred_maintenance_window = "sat:04:15-sat:04:45"
    engine_version = "5.7.mysql_aurora.2.11.1"
    auto_minor_version_upgrade = True
    license_model = "general-public-license"
    publicly_accessible = False
    storage_type = "aurora"
    copy_tags_to_snapshot = False
    db_cluster_identifier = aws_rds_db_cluster.get("resource_id")
    tags = {"Name": PARAMETER["resource_id"]}
    ret = await hub.states.aws.rds.db_instance.present(
        ctx,
        name=PARAMETER["resource_id"],
        db_instance_class=db_instance_class,
        engine=engine,
        db_subnet_group_name=db_subnet_group_name,
        availability_zone=availability_zone,
        preferred_maintenance_window=preferred_maintenance_window,
        engine_version=engine_version,
        auto_minor_version_upgrade=auto_minor_version_upgrade,
        license_model=license_model,
        publicly_accessible=publicly_accessible,
        storage_type=storage_type,
        copy_tags_to_snapshot=copy_tags_to_snapshot,
        db_cluster_identifier=db_cluster_identifier,
        tags=tags,
        **PARAMETER,
    )
