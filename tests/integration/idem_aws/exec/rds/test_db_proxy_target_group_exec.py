"""Tests for validating Rds Db Proxy Target Groups."""
import uuid

import pytest


PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="create")
async def test_create(hub, ctx, aws_rds_db_proxy):
    r"""
    **Test function**
    """

    # Test - create new resource
    global PARAMETER
    # There is no create method. Only we can update the target group.
    ret = await hub.exec.aws.rds.db_proxy_target_group.update(
        ctx,
        name=PARAMETER["name"],
        db_proxy_name=aws_rds_db_proxy.get("resource_id"),
        connection_pool_config={
            "MaxConnectionsPercent": 80,
            "MaxIdleConnectionsPercent": 10,
            "ConnectionBorrowTimeout": 60,
        },
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]
    assert aws_rds_db_proxy.get("resource_id") == resource.get("resource_id", None)

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy_target_group.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        target_group_name="default",
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert "connection_pool_config" in resource
    connection_pool_config = resource["connection_pool_config"]
    assert 80 == connection_pool_config["MaxConnectionsPercent"]
    assert 10 == connection_pool_config["MaxIdleConnectionsPercent"]
    assert 60 == connection_pool_config["ConnectionBorrowTimeout"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="get", depends=["create"])
async def test_get(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy_target_group.get(
        ctx,
        name=PARAMETER["name"],
        resource_id="invalid-resource-id",
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    ret = await hub.exec.aws.rds.db_proxy_target_group.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        target_group_name="default",
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert "connection_pool_config" in resource
    connection_pool_config = resource["connection_pool_config"]
    assert 80 == connection_pool_config["MaxConnectionsPercent"]
    assert 10 == connection_pool_config["MaxIdleConnectionsPercent"]
    assert 60 == connection_pool_config["ConnectionBorrowTimeout"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="list", depends=["create"])
async def test_list(hub, ctx, aws_rds_db_proxy):
    r"""
    **Test function**
    """

    global PARAMETER
    ret = await hub.exec.aws.rds.db_proxy_target_group.list(
        ctx,
        db_proxy_name=aws_rds_db_proxy.get("resource_id"),
        target_group_name="default",
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["list"])
async def test_update(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Update existing resource
    global PARAMETER

    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy_target_group.update(
        ctx,
        name=PARAMETER["name"],
        connection_pool_config={
            "MaxConnectionsPercent": 85,
            "MaxIdleConnectionsPercent": 15,
            "ConnectionBorrowTimeout": 60,
        },
        db_proxy_name=PARAMETER["resource_id"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy_target_group.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        target_group_name="default",
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert "connection_pool_config" in resource
    connection_pool_config = resource["connection_pool_config"]
    assert 85 == connection_pool_config["MaxConnectionsPercent"]
    assert 15 == connection_pool_config["MaxIdleConnectionsPercent"]
    assert 60 == connection_pool_config["ConnectionBorrowTimeout"]
