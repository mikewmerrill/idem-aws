from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support budget resource")
async def test_get(hub, ctx, aws_budget):
    ret = await hub.exec.aws.budgets.budget.get(
        ctx,
        name=aws_budget["name"],
        resource_id=aws_budget["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_budget["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False, "Localstack does not support budget resource")
async def test_list(hub, ctx, aws_budget):
    ret = await hub.exec.aws.budgets.budget.list(ctx)
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    budget_list = []
    for budget in ret["ret"]:
        budget_list.append(budget.get("resource_id"))
    if aws_budget["resource_id"] not in budget_list:
        assert False
    else:
        assert True
